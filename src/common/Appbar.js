import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

function Appbar() {
  let styles = {
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: 10,
    },
    title: {
      flexGrow: 1,
    },
  };
  return (
    <div className={styles.root}>
      <AppBar position="static" style={{ backgroundColor: '#C13584' }}>
        <Toolbar className="d-flex justify-content-between">
          <Typography variant="h5" align="center" className={styles.title}>
            Pinterest PhotoFeed
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default Appbar;
