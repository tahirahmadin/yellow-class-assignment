import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ArrowBackIos, ArrowForwardIos } from '@material-ui/icons';

class PhotoModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: this.props.selectedPictureId,
    };
  }
  render() {
    return (
      <div>
        <Dialog
          maxWidth={500}
          width={500}
          open={this.props.modalOpen}
          onClose={this.props.handleClose}
          aria-labelledby="max-width-dialog-title">
          <DialogTitle id="max-width-dialog-title" className="text-center">
            Picture Preview
          </DialogTitle>
          <DialogContent>
            <img src={this.props.photosList[this.state.currentIndex].urls.regular} height="500px" alt="img" />
            <div className="d-flex justify-content-between">
              <div>
                <Button
                  onClick={() => this.setState({ currentIndex: this.state.currentIndex - 1 })}
                  color="secondary"
                  style={{ outline: 'none' }}>
                  <ArrowBackIos />
                </Button>
              </div>
              <div>
                <Button
                  onClick={() => this.setState({ currentIndex: this.state.currentIndex + 1 })}
                  color="secondary"
                  style={{ outline: 'none' }}>
                  <ArrowForwardIos />
                </Button>
              </div>
            </div>
          </DialogContent>
          <div align="center">
            <Button onClick={this.props.handleClose} color="primary" style={{ outline: 'none' }}>
              Close
            </Button>
          </div>
        </Dialog>
      </div>
    );
  }
}
export default PhotoModal;
