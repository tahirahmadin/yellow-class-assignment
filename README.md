Demo Link: [Click Here](https://5f9bb9e128e21e994006da1d--yellowclass.netlify.app)

to run this project first install the dependencies by 
## npm install

and then run the project by 


## npm start

Features implemnted in this project
- Used the unsplash API to get the list of photos.
- Infinite scroll in the photo feed.
- On clicking any photo, opening that particular photo in a modal and putting arrows on the left and right side to navigate to the previous and next photo respectively. 
- Implmented Lazy load.
- Material UI and Bootstrap is used for better UI.